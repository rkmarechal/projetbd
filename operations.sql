-- Initialisation
SET serveroutput ON;


/**
 * Trigger qui lors de la suppresion d'un groupe, supprime tout les tuples qui lui sont associés dans Musiques et Albums
 * @param :OLD.g_id Identifiant du groupe supprimé
*/
CREATE OR REPLACE TRIGGER trig_suppr_grp
BEFORE DELETE ON Groupes 
FOR EACH ROW
DECLARE
  -- Déclaration de variables/cursors
  CURSOR cursor_id_albums IS 
  SELECT al_id FROM Albums WHERE g_id = :OLD.g_id; 

  -- Declaration d'un compteur de boucles
  row_id_albums cursor_id_albums%rowtype;

BEGIN
  
  -- Supprimer les tuples dans Musiques, MusicPoss, Achats et AssocAlbArt qui sont associés à l'album courant
  FOR row_id_albums IN cursor_id_albums LOOP

    -- Supprimer les tuples qui lui sont associés dans la table AssocAlbArt
    DELETE FROM AssocAlbArt WHERE al_id = row_id_albums.al_id;

    -- Supprimer les tuples qui lui sont associés dans la table MusicPoss
    DELETE FROM MusicPoss WHERE al_id = row_id_albums.al_id;

    -- Supprimer les tuples qui lui sont associés dans la table MusicPoss
    DELETE FROM Achats WHERE al_id = row_id_albums.al_id;

    -- Supprimer les tuples qui lui sont associés dans la tabl Musiques
    DELETE FROM Musiques WHERE al_id = row_id_albums.al_id;

  END LOOP;

  -- Supprimer les tuples qui lui sont associés dans la table Albums
    DELETE FROM Albums WHERE g_id = :OLD.g_id;

END;
/

/**
 * Trigger interdit la supression d'une compétence si celle-ci est présente dans AssocAlbArt
 * @param :OLD.nomi
*/
CREATE OR REPLACE TRIGGER trig_suppr_competence 
BEFORE DELETE ON Competences 
FOR EACH ROW
DECLARE
  -- Declaration de variables
  existe NUMBER;
  existe_exception EXCEPTION;
  pragma exception_init(existe_exception,-20001);
BEGIN
  -- Vérifier si cette competence existe dans AssocAlbArt
  SELECT COUNT(*) INTO existe FROM AssocAlbArt WHERE nomi = :OLD.nomi;

  -- Si existe > 0 alors empêcher la suppression
  IF (existe > 0) THEN 
    RAISE_APPLICATION_ERROR(-20001,'Erreur: competence présent dans AssocAlbArt');
  END IF;

  -- Traitement de l'exception: 
  /* PROBLEME : 
  Avec le loc EXCEPTION, le message d'erreur est plus sympa mais 
  le rollback ne s'est pas fait et les suppression ont été exécutés, ce qu'on veut éviter 
  */
  /*EXCEPTION 
    WHEN existe_exception THEN 
      --dbms_output.put_line(SQLCODE);
      dbms_output.put_line(sqlerrm);
      ROLLBACK;
*/
END;
/

/**
 * Trigger qui lors de la supression d'un client, va suprimer tout les achats qui lui sont associés
 * @param :OLD.cl_pseudo Pseudo du client qui va être supprimé
*/

CREATE OR REPLACE TRIGGER trig_suppr_client
BEFORE DELETE ON Clients 
FOR EACH ROW
DECLARE
  -- Declaration de variables
BEGIN

  -- Supprimer tout les tuples associés à ce client dans MusicPoss
  DELETE FROM MusicPoss WHERE cl_pseudo = :OLD.cl_pseudo; 

  -- Supprimer tout les achats du client qui va être supprimé
  DELETE FROM Achats WHERE cl_pseudo = :OLD.cl_pseudo;

END;
/

/**
 * Trigger qui lors qu'un client achète un album, remplir automatiquement la table MusicPoss
 * @param :NEW.cl_pseudo
 * @param :NEW.al_id
*/

CREATE OR REPLACE TRIGGER trig_achats
AFTER INSERT ON Achats
FOR EACH ROW
DECLARE
  -- Declaration de variables
  id_art_compositeur AssocAlbArt.art_id%type;
  nb_compositeurs NUMBER;

  -- Déclaration de curseurs
  CURSOR cursor_musiques IS 
  SELECT m_titre FROM Musiques WHERE al_id = :NEW.al_id;

  -- Déclaration d'un compteur de boucle pour le curseur
  row_musique cursor_musiques%rowtype;

BEGIN
  -- Initialisation
  nb_compositeurs := 0;
  id_art_compositeur := 'Artistes multiples';

  -- Compter le nombre de compositeurs 
  SELECT COUNT(*) INTO nb_compositeurs
  FROM AssocAlbArt WHERE nomi = 'aucun' AND al_id = :NEW.al_id;

  IF (nb_compositeurs > 0) THEN 
    -- Récupérer l'identifiant de l'artiste compositeur de cette album
    SELECT art_id INTO id_art_compositeur
    FROM AssocAlbArt WHERE nomi = 'aucun' AND al_id = :NEW.al_id;
  END IF;


  FOR row_musique IN cursor_musiques LOOP
    -- Insérer la musique courante dans la table MusiPoss
    INSERT INTO MusicPoss VALUES (:NEW.cl_pseudo, :NEW.al_id,id_art_compositeur,row_musique.m_titre);
  END LOOP;

END;
/

/**
 * Procédure permettant d'ajouter un tuple automatiquement dans la table AssocAlbArt pour faire le lien entre Artistes, Album et Competences
 * @param new_alb_id Identifiant de l'album qui vient d'être insérer
 * @param id_artiste Identifiant de l'artiste appartenant au groupe auteur de l'album
 * @param nom_instrument Nom de l'instrument joué par l'artiste appartenant au groupe auteur de l'album
 * @param role_artiste Rôle de l'artiste appartenant au groupe auteur de l'album 
*/
CREATE OR REPLACE PROCEDURE add_assoc_alb_art (
    new_alb_id Artistes.art_id%type,
    id_artiste Artistes.art_id%type,
    nom_instrument Competences.nomi%type,
    role_artiste Competences.role%type
)
AS
  -- Declaration de variables
   competences_exist NUMBER;
   null_id_exception EXCEPTION;

BEGIN 
  -- Initalisation
  competences_exist := 0;

  -- Procedure
  IF ((new_alb_id IS NOT NULL) AND (id_artiste IS NOT NULL)) THEN 

    -- Vérifier si la table compétences a déjà ce tuples
    SELECT COUNT(*) INTO competences_exist FROM Competences 
    WHERE nomi = nom_instrument AND role = role_artiste;

    -- Insérer uniquement si ce tuples n'existe pas déjà
    IF (competences_exist = 0) THEN 
        -- Insérer la nouvelle competence dans la table Competences
        INSERT INTO Competences VALUES (nom_instrument, role_artiste);
    END IF;

    -- Insérer un tuple dans la table AssocAlbArt  
    INSERT INTO AssocAlbArt VALUES (new_alb_id, id_artiste, nom_instrument);

  ELSE 
     RAISE null_id_exception;
  END IF;

  EXCEPTION
    WHEN null_id_exception THEN dbms_output.put_line('Erreur: Id de l''album ou de l''artiste nulle'); 

END;
/


