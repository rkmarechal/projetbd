DELETE FROM Achats;
DELETE FROM MusicPoss;
DELETE FROM Competences;
DELETE FROM AssocAlbArt;

DELETE FROM Musiques;
DELETE FROM Albums;
DELETE FROM Artistes;
DELETE FROM Groupes;

DELETE FROM Clients;

/* ****************************************************************************************************
						INSERTION TABLE CLIENTS
******************************************************************************************************* */

INSERT INTO Clients VALUES ('SYSTEM','Antonio','Mayes','gi0Ofu3p','antoniodmayes@jourrapide.com','4261248','FREE');
INSERT INTO Clients VALUES ('L3_44','Pandora','Toketa','choh9ohs6Ai','pandoratunelly@jourrapide.com','426 1249','SUBSCRIBED');
INSERT INTO Clients VALUES ('L3_48','Marcus','Carlsen','aet9Oochae','marcusmcarlsen@armyspy.com','934 22 123','FREE');
INSERT INTO Clients VALUES ('Sonfe1955','Karinka','Toteka','aeY6biet','karinkatoteka@teleworm.us','082 874 1486','SUBCRIBED');
INSERT INTO Clients VALUES ('Anorthems','Antonio','Shubin','aesh5Johku','antonioshubin@teleworm.us','9553 6043','SUBCRIBED');

/* ****************************************************************************************************
						INSERTION TABLE ARTISTES
******************************************************************************************************* */

INSERT INTO Artistes VALUES ('ar-1','Data','Yoyo');
INSERT INTO Artistes VALUES ('ar-2','Lady','Data');

/* ****************************************************************************************************
						INSERTION TABLE GROUPES
******************************************************************************************************* */

INSERT INTO Groupes VALUES ('g-1','Sextet Of The Sleek Salad',TO_DATE('12-07-1995','DD-MM-YYYY'));
INSERT INTO Groupes VALUES ('g-2','Intention Shock',TO_DATE('23-04-1983','DD-MM-YYYY'));
INSERT INTO Groupes VALUES ('g-3','Intention Shock',TO_DATE('12-07-1995','DD-MM-YYYY'));

/* ****************************************************************************************************
						INSERTION TABLE COMPETENCES
******************************************************************************************************* */

-- Remplit automatiquement

/* ****************************************************************************************************
						INSERTION TABLE ALBUMS
******************************************************************************************************* */

BEGIN 
	INSERT INTO Albums VALUES('a1','Les Douze Chimères des étoiles','00:45:00','g-1');
	add_assoc_alb_art('a1','ar-1','guitare','musicien');
	add_assoc_alb_art('a1','ar-2','trompette','musicien');

	INSERT INTO Albums VALUES('a2','La mort est un lapin multicolore','00:45:00','g-2');
	add_assoc_alb_art('a2','ar-1','aucun','compositeur');

	INSERT INTO Albums VALUES('a3','La mort est un lapin multicolore','00:58:00','g-1');
	add_assoc_alb_art('a3','ar-2','guitare','musicien');

	INSERT INTO Albums VALUES('a4','Les Douze Chimères des étoiles','00:45:58','g-2');
	add_assoc_alb_art('a4','ar-2','aucun','compositeur');

	INSERT INTO Albums VALUES('a5','La copie c''est fantastique','00:23:24','g-3');
END;
/
COMMIT;

/* ****************************************************************************************************
						INSERTION TABLE MUSIQUE
******************************************************************************************************* */

INSERT INTO Musiques VALUES('La flemme','a1','00:05:03');
INSERT INTO Musiques VALUES('La BDD de folie','a1','00:04:00');
INSERT INTO Musiques VALUES('La flemme','a2','00:04:00');
INSERT INTO Musiques VALUES('La flemme', 'a3','00:05:03');
INSERT INTO Musiques VALUES('La BDD de folie','a4','00:04:00');

/* ****************************************************************************************************
						INSERTION TABLE ASSOCALBART
******************************************************************************************************* */

/*
La procédure add_assoc_alb_art se charge de d'insérer automatiquement les tuples de cette table

Pour vérifier le contenu après son exécution:

select * from competences;
select * from AssocAlbArt;

*/


/* ****************************************************************************************************
						INSERTION TABLE MUSICPOSS
******************************************************************************************************* */

-- Remplit automatiquement

/* ****************************************************************************************************
						INSERTION TABLE ACHATS
******************************************************************************************************* */

INSERT INTO Achats VALUES('a1','L3_48',TO_DATE('02-11-2014','DD-MM-YYYY'));
INSERT INTO Achats VALUES('a2','L3_44',TO_DATE('02-11-2014','DD-MM-YYYY'));
INSERT INTO Achats VALUES('a3','Sonfe1955',TO_DATE('03-01-2015','DD-MM-YYYY'));
INSERT INTO Achats VALUES('a1','Sonfe1955',TO_DATE('04-01-2015','DD-MM-YYYY'));
INSERT INTO Achats VALUES('a1','Anorthems',TO_DATE('01-05-2015','DD-MM-YYYY'));


