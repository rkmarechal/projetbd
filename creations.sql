DROP TABLE Clients CASCADE CONSTRAINTS;
DROP TABLE Artistes CASCADE CONSTRAINTS;
DROP TABLE Groupes CASCADE CONSTRAINTS;
DROP TABLE Competences CASCADE CONSTRAINTS;
DROP TABLE Albums CASCADE CONSTRAINTS;
DROP TABLE Musiques CASCADE CONSTRAINTS;
DROP TABLE AssocAlbArt CASCADE CONSTRAINTS;
DROP TABLE MusicPoss CASCADE CONSTRAINTS;
DROP TABLE Achats CASCADE CONSTRAINTS;


/**
 * Table contenant tout les tuples des clients identifié par leur speudo
*/
CREATE TABLE Clients (
	cl_pseudo VARCHAR2(40),
	cl_nom VARCHAR2(40),
	cl_prenom VARCHAR2(40),
	cl_passw VARCHAR2(40),
	cl_mail VARCHAR2(40),
	cl_tel VARCHAR2(40),
	cl_statut VARCHAR2(40),
	CONSTRAINT PK_Clients PRIMARY KEY (cl_pseudo)
);

/**
 * Table contenant tout les tuples des artistes identifiés par art_id
*/
CREATE TABLE Artistes (
	art_id VARCHAR2(40),
	art_nom VARCHAR2(40),
	art_prenom VARCHAR2(40),
	CONSTRAINT PK_Art PRIMARY KEY (art_id)
);

/**
 * Table contenant tout les tuples des Groupes
*/
CREATE TABLE Groupes (
	g_id VARCHAR2(40),
	g_nom VARCHAR2(40),
	g_datecrea DATE,
	CONSTRAINT PK_Grp PRIMARY KEY (g_id)
);


/*
 * Tables contenant les compétences de Artistes (nom d'instrument et leur role)
 * Remarque: on ne peut pas faire de clé étrangère de Competence vers AssocAlbArt ni de AssocAlbArt car 
   les clé primaire dans ces deux tables sont composés.
*/
CREATE TABLE Competences (
	nomi VARCHAR2(40),
	role VARCHAR2(40),
	CONSTRAINT PK_Compt PRIMARY KEY (nomi,role)
);

/**
 * Table contenant tout les tuples des Albums
*/
CREATE TABLE Albums (
	al_id VARCHAR2(40) NOT NULL,
	al_nom VARCHAR2(40),
	al_duree VARCHAR2(8),
	g_id VARCHAR2(40),
	CONSTRAINT PK_Alb PRIMARY KEY (al_id),
	CONSTRAINT FK_Alb_grp FOREIGN KEY (g_id) REFERENCES Groupes (g_id)
);


/*
* Table contenant les tuples des Musiques
* La clé primaire est composé de m_titre et al_id car m_titre seul e peut être une clé primaire étant donné que 
  plusieurs musiques peuvent avoir le même titre.
*/
CREATE TABLE Musiques (
	m_titre VARCHAR2(40),
	al_id VARCHAR2(40),
	m_duree VARCHAR2(8),
	CONSTRAINT PK_Mus PRIMARY KEY (m_titre,al_id),
	CONSTRAINT FK_Mus_alb FOREIGN KEY (al_id) REFERENCES Albums(al_id)
);

/*
 * Table permettant d'associer un album à un artiste 
 * Clé primaire composé des deux clés étrangères al_id et art_id. 
 * Remarque: on ne peut pas faire de clé étrangère de AssocAlbArt vers Competences 
   ni de Competences vers AssocAlbArt car les clé primaire dans ces deux tables sont composés.
*/
CREATE TABLE AssocAlbArt (
	al_id VARCHAR2(40) NOT NULL,
	art_id VARCHAR2(40) NOT NULL,
	nomi VARCHAR2(40),
	CONSTRAINT PK_AssocAlbArt PRIMARY KEY (al_id, art_id),
	CONSTRAINT FK_AssocAlbArt_alb FOREIGN KEY (al_id) REFERENCES Albums(al_id),
	CONSTRAINT FK_AssocAlbArt_art FOREIGN KEY (art_id) REFERENCES Artistes(art_id)
);

/**
 * Table représentant les musiques possédés par les clients en faisant le lien 
   entre les clients, l'album, l'artiste et le nom de la musique
*/
CREATE TABLE MusicPoss (
	cl_pseudo VARCHAR2(40),
	al_id VARCHAR2(40),
	art_id VARCHAR2(40),
	m_titre VARCHAR2(40),
	CONSTRAINT PK_MuicPoss PRIMARY KEY (cl_pseudo,al_id, art_id,m_titre),
	CONSTRAINT FK_MusicPoss_pseudo FOREIGN KEY (cl_pseudo) REFERENCES Clients(cl_pseudo),
	CONSTRAINT FK_MusicPoss_alb FOREIGN KEY (al_id) REFERENCES Albums(al_id)
	--CONSTRAINT FK_MusicPoss_art FOREIGN KEY (art_id) REFERENCES Artistes(art_id)
);

/**
 * Table contenant les albums acheté par les clients
 * La clé primaire est composé (al_id,cl_pseudo) ceci permet de garantir qu'un client ne peut acheter le même 
   album plus d'une fois.
*/
CREATE TABLE Achats (
	al_id VARCHAR2(40),
	cl_pseudo VARCHAR2(40),
	dateachat DATE,
	CONSTRAINT PK_Achats PRIMARY KEY (al_id, cl_pseudo),
	CONSTRAINT FK_Achats_alb FOREIGN KEY (al_id) REFERENCES Albums(al_id),
	CONSTRAINT FK_Achats_cli FOREIGN KEY (cl_pseudo) REFERENCES Clients(cl_pseudo)
);




