/* *****************************************************************************************************
									TEST des vues
***************************************************************************************************** */

/*
Ceci fonctionnera unqiuement si connecté en tant que utilisateurs SYSTEM ou L3_48 (rôle client)
*/
SELECT * from V_CLIENT_MUSIQUE;

/*
Ceci doit fonctionner uniquement si connecté en tant que utilisateurs L3_44 (rôle artiste)
*/
-- Ajout d'un nouveau groupe
INSERT INTO Groupes VALUES ('g-4','GroupeTest',TO_DATE('20-03-2018','DD-MM-YYYY'));

-- Ajout d'un album
BEGIN 
	INSERT INTO Albums VALUES('a6','AlbumTest','00:55:00','g-4');
	add_assoc_alb_art('a6','ar-1','guitare','musicien');
END;
/
COMMIT;

-- Ajout d'une musique dans cette album
INSERT INTO Musiques VALUES('MusiqueTest','a6','00:07:01');


/* *****************************************************************************************************
									TEST du trigger trig_achats :
***************************************************************************************************** */

/* 
Ici on veut vérifier que le trigger a bien défini le compositeur pour art_id dans le cas où il 
existe un artiste ayant le rôle de compositeur dans ce groupe. Sinon mettre 'Artistes multiples'
*/

SELECT art_id FROM MusicPoss; 


/* *****************************************************************************************************
									TEST du trigger trig_suppr_grp :
***************************************************************************************************** */

/*
Test enchaînement de suppression manuel:
DELETE FROM AssocAlbArt WHERE al_id = 'a1' OR al_id='a3';
DELETE FROM Musiques WHERE al_id = 'a1' OR al_id='a3';
DELETE FROM Albums WHERE g_id = 'g-1';
*/

-- Afficher les tuples de AssocAlbArt qui sont associés à l'album d'id g-1:
SELECT * FROM AssocAlbArt WHERE al_id = 'g-1';

-- Afficher les tuples de Musiques qui sont associés à l'album d'id g-1:
SELECT m_titre,al_id FROM Musiques WHERE al_id IN (
	SELECT al_id FROM Albums WHERE g_id = 'g-1');

-- Afficher les tuples de Albums qui sont associés à l'album d'id g-1
SELECT al_id,al_nom FROM Albums WHERE g_id = 'g-1'; 

-- Supprimer le groupe g-1:
DELETE FROM Groupes WHERE g_id = 'g-1';

-- Afficher les tuples de AssocAlbArt qui sont associés à l'album d'id g-1:
SELECT * FROM AssocAlbArt WHERE al_id = 'g-1';

-- Afficher les tuples de Musiques qui sont associés à l'album d'id g-1:
SELECT m_titre FROM Musiques WHERE al_id IN (
	SELECT al_id FROM Albums WHERE g_id = 'g-1');

-- Afficher les tuples de Albums qui sont associés à l'album d'id g-1
SELECT al_id,al_nom FROM Albums WHERE g_id = 'g-1'; 


-- Supprimer le groupe g-2:
DELETE FROM Groupes WHERE g_id = 'g-2';
SELECT * FROM AssocAlbArt WHERE al_id IN 'g-2';
SELECT m_titre FROM Musiques WHERE al_id = (
	SELECT al_id FROM Albums WHERE g_id = 'g-2');
	SELECT al_id,al_nom FROM Albums WHERE g_id = 'g-2'; 


/* *****************************************************************************************************
									TEST du trigger trig_suppr_competence :
***************************************************************************************************** */

-- SELECT * FROM Competences;

/* *****************************************************************************************************
									TEST du trigger trig_suppr_client :
***************************************************************************************************** */

DELETE FROM Clients WHERE cl_pseudo = 'SYSTEM';

/* *****************************************************************************************************
									TEST de la procédure add_assoc_alb_art
***************************************************************************************************** */

/*
Utilisé dans fichier insertion.sql
*/

